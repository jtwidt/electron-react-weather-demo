import ReactWeather, { useOpenWeather } from 'react-open-weather';
import './App.css';

function App() {
  const { data, isLoading, errorMessage } = useOpenWeather({
    key: '87e824235cda369304a6af239d9cbae0',
    lat: '29.4241',
    lon: '-98.4936',
    lang: 'en',
    unit: 'imperial', // values are (metric, standard, imperial)
  });

  return (
    <div className='App'>
      <ReactWeather
        isLoading={isLoading}
        errorMessage={errorMessage}
        data={data}
        lang='en'
        locationLabel='San Antonio, Texas'
        unitsLabels={{ temperature: 'F', windSpeed: 'mph' }}
        showForecast
      />
    </div>
  );
}

export default App;
